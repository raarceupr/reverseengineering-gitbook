# Usando OllyDbg para esquivar el mecanismo de licencia

En esta ocasión, vamos a analizar el programa "mIRC32" y modificar su ejecutable para esquivar su mecanismo de validación de licencia.

Todos los pasos de este ejercicio serán llevados a cabo desde Windows.

1. Descarga el instalador de "mIRC32" desde: http://ccom.uprrp.edu/~rarce/mirc571t.exe. Si chrome no te deja, usa wget desde cygwin. Instala la aplicación mIRC usando las opciones default.

1. Navega hasta el directorio donde quedó instalado el ejecutable de la aplicación y crea una copia de resguardo (por si los cambios que haremos dañan el ejecutable)

1. Corre OllyDbg y abre el ejecutable de mIRC32. Córrelo desde OllyDbg. Cierra la ventanita de configuración que abre mIRC para que puedas ir al menú “Help” (en mIRC) y escoger “Register..”. . Entra palabras memorables (para ti) en los espacios de “Full Name” y “Registration Code”.

1. Queremos encontrar cuál es la función que determina si una clave es válida. Comenzamos por investigar dónde en memoria quedaron guardadas las palabras que escribimos en la ventana de inscripción. Abre la ventana de Memoria (botón azul con la letra ‘M’) y realiza una búsqueda por la palabra que usaste para el nombre o código.

1. Cuando encuentres la palabra. Marca el comienzo de la palabra en la ventana de memoria y haz right-click y escoge la opción “Breakpoint - Hardware, on access - byte”. Regresa al programa y accede de nuevo la ventanita de register. Al entrar la información y dar click al botón de Register debes obtener la ventana de OllyDbg avisando que ha llegado a un breakpoint.

1. Debes notar que la ventana de CPU dice que el programa ha sido detenido en el módulo ntdll. Esto quiere decir que el programa mIRC32 invocó alguna de las funciones de la librería ntdll para que esta se hiciera cargo de leer y escribir el contenido de los textboxes.

1. Ya que no necesitamos el breakpoint, vamos a eliminarlo. Haz “Debug - Hardware breakpoints” y elimínalo.

1. No nos interesa entender el código de la función de ntdll si no averiguar qué destino corren los datos que se leen de los textboxes. Por esto, escoje “Debug - Execute till user code” hasta que la ventana de CPU diga “main thread, module mirc32”. Observa las instrucciones alrededor de donde está detenido el programa. ?Qué función fue la que se invocó para leer los textboxes?

1. Ahora observa unas cuantas instrucciones más abajo donde se colocan como parámetro los strings leídos antes de invocar a cierta función. ?Qué tal si esa es la función que valida los códigos? Anota la dirección de ese CALL y averigua lo que está devolviendo. Recomiendo que hagas step a través de las instrucciones. Ten en cuenta lo que devuelve la llamada para que hagas que el programa evite la ruta normal (por ejemplo, si el valor devuelto es 0 y esto hace que se brinque, debes hacer que no brinque).

1. Cambia la(s) instruccion(es) necesaria(s) para que el programa esquivé la ruta del dolor. Luego de cambiarla(s) debes dar run (más de una vez?). Eventualmente, si has hecho todo bien veras....

    ![](/assets/olly00.png)

1. Descarga algún editor de hex para windows para que hagas el cambio permanente.


