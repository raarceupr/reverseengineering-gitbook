# Analizando programas en C

Analiza los siguiente programas para determinar qué datos debes entrar para obtener el mensaje de éxito.

### Programa 1

```c
#include <stdio.h>
#include <unistd.h>
#include <string.h>

int main(){
  char p0[256], p1[256], p2[256];
  int a, b;

  printf("Please password 0: "); scanf("%s", p0);

  printf("Please password 1: ");
  scanf("%s", p1);

  printf("Please password 2: ");
  scanf("%s", p2);

  a = strcmp(p0,p1);
  b = strcmp(p1,p2);

  if( ( ( a ^ b ) & (char) p0[1] ) == 0xF)
    printf("You gained access!!!.\n");
  else
    printf("I'm sorry, that is invalid. Please restart your computer :-P\n");

  return 0;
}
```


### Programa 2

```c
#include <stdio.h>
#include <unistd.h>
#include <string.h>

int main() {
  char buf[256];
  int ok = 0;
  char tmp = 0;

  printf("Please the secret registration number: ");
  scanf("%s", buf);
  for(int i = 0; i < strlen(buf); i++) {
    tmp = buf[i] & 0xF;
    ok = ok << 4;
    ok ^= tmp;
  }

  ok += 1;

  if(ok == 0xdeadbeef)
    printf("Success!!!.\n");
  else
    printf("Failure :-(.\n");

  return 0;
}

```


### Programa 3

```c
#include <stdio.h>
#include <string.h>

int main(){
  char buf[256];
  char tmp = 0;
  short a, b;

  printf("Please the secret registration numbers: ");
  gets(buf);

  int k = sscanf(buf,"%hd %*hd %hd", &a, &b);

  if (k == 2 && (a & 0x100) && (b & 0x100) && (a + b) << 1 == 0xcefa)
    printf("Success!!!.\n");
  else
    printf("Failure :-(.\n");

  return 0;
}
```

### Entregables 

Entregue un documento pdf donde muestre su análisis de los programas.
