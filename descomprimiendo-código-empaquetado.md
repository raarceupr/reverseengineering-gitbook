# Descomprimiendo código empaquetado

La compresión de ejecutables es una técnica que surgió cuando la capacidad de los medios de almacenamiento era limitada en comparación con los tamaños de los ejecutables. Hoy día no existen tales limitaciones, pero la compresión puede ser utilizada como técnica para disuadir la ingeniería inversa de software. Esto se debe a que el ejecutable resultante requiere de pasos adicionales para ser analizado.

En esencia, un empaquetador de ejecutables comprime el contenido del ejecutable y lo combina con el código de descompresión para crear un nuevo ejecutable. Cuando el ejecutable comprimido es ejecutado, el código de descompresión recrea el código original y luego lo ejecuta.

![](/assets/compress00.png)  **Figura 1**. Al ser ejecutado el ejecutable comprimido, este descomprime el ejecutable original y dirige la ejecución al punto de entrada original.

En este ejercicio vas a analizar un ejecutable que ha sido comprimido usando el empaquetador ASPack (http://www.aspack.com/). En pocas palabras, la estrategia será, usando un debugger, permitir que el programa ejecute hasta el punto donde el ejecutable original ha sido descomprimido. A partir de ese punto podrías continuar con un análisis parecido al que hemos llevado a cabo en otros ejercicios.

### Herramientas de software

PEID, OllyDbg (o IDA)

### Procedimiento

1. Descargar http://shell-storm.org/repo/CTF/SuCTF-2014-quals/200-reverse-me/ReverseMe.exe y usar PEId para corroborar que en efecto es un ejecutable comprimido. Anota el EntryPoint y el rango de direcciones donde vive el código del ejecutable (según PEId). Observa que el EntryPoint no queda en la sección .text si no en una sección llamada .aspack.

1. Abre ReverseMe.exe en OllyDbg. Notarás que OllyDbg se dió cuenta que el Entry Point queda fuera del .text y te lo notifica.

    ![](/assets/compress01.png)

1. Una vez en OllyDbg, nota que la dirección debe corresponder al entry point que observaste en PEId (al menos de forma relativa). En otras palabras, si el entry point según PEId es un desplazamiento de 1 a partir de la sección de .aspack, también debe serlo en OllyDbg. Explica cómo corroborar esta información.

1. La función donde comienza el programa es la encargada de descomprimir el programa original. Dado que cuando comencemos a ejecutar el programa original quisiéramos que el microprocesador se encuentre en exactamente el mismo estado que cuando se invocó el programa comprimido, la primera labor del descompresor es almacenar el estado de los registros usando la instrucción pushad. Luego realiza la descompresión y hace un popad para restablecer el estado original antes de invocar el código descomprimido.

1. Permite que el programa ejecute hasta pushad (inclusive) y crea un hardware breakpoint a alguna de las direcciones del stack donde pushad guarda los valores de los registros. La intención es que el breakpoint se active en la instrucción popad y nos revele las instrucciones donde se invoca el código original.

1. Continúa la ejecución del código para que se active el breakpoint. Se supone que el código se ha detenido justo después de ejecutar el popad. Lleva el programa paso a paso hasta que notes que el programa ha brincado a una dirección de la sección .text.  
    * ¿Cuál es el entry point del programa descomprimido? 
    * ¿Cómo puedes corroborar que te encuentras en la sección .text?

1. Si haces ‘CTRL-A’ OllyDbg hará un disassembly de la sección .text para mostrarte las instrucciones en assembly. Supuestamente existen pluggins para OllyDbg que serían capaces de guardar el ejecutable descomprimido en un file de formato PE. Lamentablemente, no he logrado que me funcionen :-(. Por suerte el programa ReverseMe.exe usa un GUI y mientras corre podemos ir analizándolo para entenderlo.

8. Usa el programa y nota su comportamiento. Algunas cosas que puedes tratar:

   * Investigar desde dónde es llamada las función MessageBox o MessageBoxA.  
   * ¿Cuáles son las instrucciones previas a esa llamada?
   * ¿Qué condiciones llevarían al programa a seguir un flujo distinto, i.e. aceptar el email/serial number en vez de rechazarlo.

9. Averigüa al menos: 

    * ¿Cuántos caracteres debe contener el serial code? 
    * ¿Cuál debe ser el primer y el último caracter?

### Entregables

A través de un link que se proveerá en Moodle, somete un zip que contenga:

* Un documento que describa el análisis que hiciste y las contestaciones de las preguntas del paso 9.


