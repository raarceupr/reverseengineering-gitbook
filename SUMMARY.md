# Summary

* [Introduction](README.md)
* [Analizando programas en C](analizando-programas-en-c.md)
* [El stack](el-stack.md)
* [Editando el mecanismo de licensia de un software](editando-el-mecanismo-de-licensia-de-un-software.md)
* [Usando OllyDbg para esquivar el mecanismo de licencia](usando-ollydbg-para-esquivar-el-mecanismo-de-licencia.md)
* [Usando IDA para reversar un keygen](usando-ida-para-reversar-un-keygen.md)
* [Implementando un code cave](implementando-un-code-cave.md)
* [Inspeccionando Malware](inspeccionando-malware.md)
* [Reversando Java y C\#](reversando-java-y-c.md)
* [Reversando para usar librerías](reversando-para-usar-librerias.md)
* [Infecting a binary with malicious code](infecting-a-binary-with-malicious-code.md)
* [Stack overflow](stack-overflow.md)

