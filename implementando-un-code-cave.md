# Implementando un codecave

Un codecave se puede definir como una redirección en la ejecución de un programa a otra localización y que luego regresa al punto donde fue interrumpida la ejecución. La redirección se realiza para introducir funcionalidad nueva al programa sin alterar el resto de las funcionalidades originales.

En este ejercicio vas a alterar el programa notepad para que antes de mostrar su pantalla imprima un message box con un mensaje de advertencia sobre notepad, e.g. “En serio, todavía estas usando notepad?? :-)”.

### Herramientas de software

PEID, OllyDbg (o IDA)

### Procedimiento





1. Haz una copia del programa notepad (C:/windows/system32/notepad.exe) a otro directorio. Trabajaramos sobre esa copia.

1. Para planificar un codecave hay que primero determinar dónde será posible implementarlo. Usando PEId y OllyDbg averigua la información de las primeras dos filas de la siguiente tabla. Luego decide dónde comenzarás el codecave y llena la tercera fila. Es recomendable comenzar en una dirección “bonita” (que termine en 0) por si tienes que realizar cómputos de direcciones más adelante.

    |Programa: notepad.exe | Según PEId | En el archivo | En memoria (según OllyDbg u otro debugger) |
    |--|--|--|--|
    | Dirección donde comienza la sección de código | | | |
    | Dirección del **entry point** | | | |
    | Dirección donde puede comenzarás el codecave| | | |

1. Ahora debes decidir dónde en el código original desviarás la ejecución hacia el codecave. Si la intención es que notepad imprima un mensaje antes de mostrar la ventana principal, debes conocer la función del API Windows que crea la ventana principal: CreateWindowExW. Localiza la invocaciones a dicha función y decide dónde pondrás la instrucción de call al codecave. Preferiblemente debes escoger substituir otra instrucción que ocupe igual número de bytes que el call. En tu informe indica cuál fue la instrucción que sustituiste y muestra las instrucciones que tiene alrededor.

1. Diseña la función que implementará la maldad. No tienes que ser un experto en assembly para poder implementar la funcionalidad de crear un message box. Simplemente copia y adapta alguna de las invocaciones que ya realiza el programa. Recuerda que al regresar de la función del codecave el estado del micro debe estar exactamente igual que cómo iba a estar originalmente, de otra forma te arriesgas a que el resto del programa funcione erráticamente.

1. Utiliza OllyDbg o IDA para probar tu estrategia de codecaving en memoria. Una vez funcione haz patching al ejectable para implementar el codecave.

### Entregables

A través de un link que se proveerá en Moodle, someta un zip que contenga:

1. Un documento que describa el análisis que hiciste y la estrategia que usaste para introducir el codecave

2. El archivo notepad.exe alterado.


