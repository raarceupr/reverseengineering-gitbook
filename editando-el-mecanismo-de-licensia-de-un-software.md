# Editando el mecanismo de licencia de un software

Vamos a analizar el programa "MP3 Cutter Joiner" y modificar su ejecutable para esquivar su mecanismo de validación de licencia. Usaremos ollydbg, un debugger y analizador para windows que nos ofrece muchas funcionalidades y pantallas para darle seguimiento al ejecutable.

Todos los pasos de este ejercicio serán llevados a cabo desde Windows.

1. Descarga el archivo Odbg110.zip de http://www.ollydbg.de/ y descomprimelo. No hay que correr ningun instalador para usar Ollydbg. Existe una versión más reciente del debugger "Odbg" pero aun no la he validado para propósitos de este ejercicio.

1. Descarga el instalador de "MP3 Cutter Joiner" desde: http://ccom.uprrp.edu/~rarce/mp3cutterjoiner.exe . Instala la aplicación usando las opciones default.

1. Navega hasta el directorio donde quedó instalado el ejecutable de la aplicación y crea una copia de resguardo (por si los cambios que haremos dañan el ejecutable).

1. Corre "MP3 Cutter Joiner" y trata cualquier "Register Name" y "Register Code", seguido oprime “Register”. Para simplificar el ejercicio vamos a asumir que hemos averiguado que el register code debe consistir de números.

1. Obtendrás una ventana con el mensaje "Invalid register code! Please retry!". Ese mensaje es importante pues será nuestro punto de partida para empezar el análisis.

1. Cierra "MP3 Cutter Joiner", pues ahora lo vamos a volver a abrir desde OllyDbg para poder analizarlo.

1. Ejecuta OllyDbg **como administrador** (Run as administrator). De esta forma podrás usar todas las funcionalidades del debugger.

1. Escoge "File->Open" y abre **el ejecutable** de "MP3 Cutter Joiner" que probablemente encontrarás en `C:\Program Files\SuperAudioTool\MP3 Cutter Joiner\MP3 Cutter Joiner.exe`. Nota que en la esquina de OllyDbg indica que el programa está pausado.

1. Sabemos que una situación que deseamos evitar es que MCJ nos anuncie que hemos entrado una clave inválida. Así que antes de ejecutarlo desde OllyDbg vamos a realizar una búsqueda por el string "Invalid register code! Please retry!". Presiona el botón azul "M" para que puedas ver la pantalla de *Memory map*. Dicha pantalla muestra la distribución en memoria de las secciones del programa. Entre ellas puedes ver algunas de los que hemos visto en otros ejercicios, tales como .text, .data, .rdata.

    ![](/assets/lic05.png)  

    **Figura 1** - Ventana memory map.

1. Para realizar la búsqueda accesa el menú del botón derecho y escoge "Search" (alternativamente puedes teclear Ctrl-B). Haremos una búsqueda para ver dónde en memoria está escrito dicho string.

    ![](/assets/lic02.png)  
    
    **Figura 2** - Ventana para búsqueda.

1. Anota la dirección donde encontraste el string. La usaremos para determinar qué instrucciones del programa la referencian.

    ![](/assets/lic00.png)  

    **Figura 3** - Ventana “Dump” para ver contenido de memoria.

1. En la ventana de “CPU” haga una búsqueda por la dirección donde halló el string. Active un breakpoint (Toggle) en las tres instancias en que aparece referenciada la dirección del string. Estos breakpoints nos ayudarán a analizar la secuencia de pasos que sigue el programa para notificar al usuario sobre la invalidez de su clave. La siguiente figura muestra la forma una de las instrucciones que encontró la búsqueda. Note que OllyDbg crea comentarios al lado de algunas instrucciones para ayudar al usuario a entender la lógica del programa.

    ![](/assets/lic01.png)  

    **Figura 4** - Ventana “CPU” mostrando una de las instrucciones donde se hace referencia al string “Invalid register code”.

1. En este punto debe haber identificado tres instrucciones que hacen referencia al string y debe haberlas activado como breakpoints. Para ver una lista de los breakpoints, puede abrir la ventana de breakpoints usando “View->Breakpoints”.

    ![](/assets/lic06.png)  

    **Figura 5** - Ventana donde se muestran los breakpoints añadidos al programa.

1. Escoge la opción de “Run” para que se ejecute MCJ. Luego de algunos segundos verifica si el programa ya está disponible para interaccionar con el usuario.

1. Verás que MCJ abrió su ventana “Register”. Escribe cualquier cosa en el “Register Name” y cualquier número en el “Register Code” y oprime “Register”.

    ![](/assets/lic03.png)  

    **Figura 6** - Debes ver esto cuando comienzas a ejecutar MJC desde OllyDbg.

1. En la ventana “CPU” de OllyDbg se supone que verás que el programa ha sido detenido en uno de los breakpoints que especificaste anteriormente. Analiza las instrucciones que vienen antes del breakpoint para que determines si hay una instrucción que puedas modificar de modo que nunca se ejecute la instrucción del breakpoint. Hint: préstale atención a las instrucciones `je` y `cmp`. Para modificar la instrucción debes escogerla y apretar la tecla de espacio. Obtendrás una ventana como la siguiente, en la que puedes escribir directamente en assembly la instrucción por la que deseas sustituir.

 ![](/assets/lic07.png)

 **Figura 7** - Ventana para modificar una instrucción del programa.

1. Regresa a la ventana de MCJ y verás el popup de “Invalid register...”.

18. Ahora vamos a probar si el parche que pusiste en el programa al menos esquiva el primer breakpoint. Cliquea “Register” en MCJ y verifica en dónde se detuvo el programa. La buena noticia es que saltaste una de las situaciones que invoca al popup maldito. La mala noticia es que el programa todavía cae en otro flujo que lo lleva al popup maldito.

19. Observa en la instrucción donde se detuvo y nota que OllyDbg pone un “.” al lado de la mayoría de las instrucciones y un “>” al lado de otras. Las instrucciones rotuladas con “>” son instrucciones a las que se brinca desde algún otro lugar en el programa. Así que si deseamos averiguar cómo llegamos hasta aquí buscamos la primer instrucción antes del breakpoint que tiene un “>”. Si escoges (haces click) sobre esa instrucción, obtendrás información sobre la dirección donde está el jump que te lleva esa instrucción.

1. Analiza la lógica y modifica el programa de modo que esquives este breakpoint. Si lo haces bien y vuelves a MCJ y oprimes “Register” debes obtener un mensaje de felicitación. Luego en MCJ si escoges “Help->About” verás que el software piensa que ha sido registrado!

    ![](/assets/lic04.png)   
    
    **Figura 8** - Dulce sabor del éxito.


