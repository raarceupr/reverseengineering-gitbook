# Usando IDA para reversar un KeyGen


> [Wikipedia] Un keygen (del inglés key generator, generador de claves) es un programa informático que al ejecutarse genera un código (serial) para que un determinado programa de software de pago en su versión de prueba (Trial) pueda ofrecer los contenidos completos del programa ilegalmente y sin conocimiento del desarrollador.


1. Obtenga el TDC KeyGenME haciendo un

    ```
wget http://www.reversing.be/binaries/articles/20060115002634593.zip
    ```
    (Chrome bloquea la transferencia por tratarse de un zip con ejecutables dentro).

2. Descomprima el zip y los zips que tiene dentro. El programa `keygenme.exe` está en el directorio TDC8. 
    ![](assets/keygen01.png)
    
    **Figura 1**- Interfaz del KeyGen

3. Espere que IDA complete el análisis. IDA construye una base de base de datos usando las informaciones que deduce el ejecutable.

    En la “Names Window” busque GetWindowTextA y haga doble click.

    ![](/assets/keygen02.png)

    **Figura 2**- Parte del código donde está el jump a la función GetWindowTextA

1. Para conocer cuáles son las invocaciones que hace el programa a GetWindowTextA, marque el título de la función y presione la tecla ‘X’ (que invoca la opción ‘Jump to xref to operand’). En la ventana que obtiene deben aparecer los dos CALL’s que hay en el programa a la función GetWindowTextA. Puede ir a cualquiera de ellos dando doble click.

    ![](/assets/keygen03.png)

    Figura **3** - Referencias a la función GetWindowTextA

    Por ejemplo, si da doble click el primero de los CALLs, IDA le llevará al disassembly de esa instrucción. Observe que ambos CALLs están próximos uno al otro. Además observe las anotaciones que ha hecho IDA en las instrucciones que preceden cada CALL. IDA tiene una librería de prototipos de funciones de windows y cada vez que identifica una invocación a una función conocida crea anotaciones para los parámetros. Por ejemplo, IDA conoce que el prototipo de GetWindowText es:

    ```
int WINAPI GetWindowText(HWND hWnd, LPTSTR lpString, int nMaxCount);
    ```

    , así rotula con hWnd el argumento que es puesto en el tope del stack, rotula con lpString el segundo argumento, etc.

    ![](/assets/keygen04.png)

    **Figura 4**. Pedazo donde se encuentran las llamadas a GetWindowTextA.

    Según la figura 4, ¿en qué direcciones están las dos variables que contendrán el name y serial que pide el KeyGenc

    ¿Cómo podrías verificar que la primer GetWindowTextA recoge el name y la segunda el serial? Corrobora tu contestación en IDA.

1. Ahora observa la ‘Strings Window’ y busque el string “Solved! How did you do?..”. Haga doble-click e IDA le mostrará la memoria donde está guardado.

    ![](/assets/keygen06.png)

    **Figura 5**- Región de memoria donde vive el string “Solved! How did you?...”

    Además, te muestra mediante un link, la dirección de la instrucción donde se hace referencia al string (en este caso una línea que IDA ha identificado como DialogFunc+17A). Si das click en ese link, IDA te lleva a esa parte del programa.

    )![](/assets/keygen07.png)

    **Figura 7** - Código donde se referencia el string “Solved! How did you?...”


1. Analiza el flujo del programa para saber cuáles son las llamadas que se hacen desde que el programa lee el user y serial hasta que imprime el mensaje de éxito. Tu tarea NO es patchear el programa para llegar a la pantalla de éxito. Tu tarea es comprender las funciones para poder explicarle a un usuario qué información debe poner en user y serial para ser exitoso.

    Por ejemplo, ¿qué caracteres puede contener el user?¿cómo se computa el serial en base al user?



### Otros shortcuts útiles:

* X : referencias a una función

* Space: alterna entre ‘Graph’ y ‘Text View’.

* Esc: regresa a vista anterior

* F2: añadir breakpoint

* F9: Comenzar debugger, continuar

* Ctrl-F2: Terminar debug

* F7: Step into

* F8: Step over

* Alt-A: convertir a string


### Entregables

A través de un link que se proveerá en Moodle, someta un zip que contenga:

1. Un documento que describa el análisis que realizó a las funciones de la librería (paso 1)

2. El archivo .cpp donde invoca las funciones (paso 2)


